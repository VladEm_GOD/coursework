﻿using CourseWork.Classes;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Windows.Threading;

namespace CourseWork.Pages
{
    /// <summary>
    /// Логика взаимодействия для Page2.xaml
    /// </summary>
    public partial class Page2 : Page
    {
        Planet Sun, Earth;
        double AUInOneVect = 0.1;
        double dt = 0.001; 
        static Point oldPos, newPos;
        double degX, degY;
        bool isFirst = true, ReadyForRotating = false;
        public Page2()
        {
            InitializeComponent();
            MoveObj.IsEnabled = false;
        }

        private void MoveObj_Click(object sender, RoutedEventArgs e)
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = new TimeSpan(0, 0, 0, 0, 1);
            timer.Start();
            MoveObj.IsEnabled = false;
            Scene.Focus();

        }

        private void GenerateButton_Click(object sender, RoutedEventArgs e)
        {
            Init();

            Vector3D LenthToCent = new Vector3D(Earth.X, Earth.Y, Earth.Z);
            PlanetsDataTextBlock.Text = Earth.X.ToString() + "\n" + Earth.Y.ToString() + "\n" + Earth.Z.ToString() + "\n" + "DistToSun:" + (LenthToCent.Length*AUInOneVect).ToString();

            PositionXTextBox.Text = "X: " + Earth.X.ToString();
            PositionYTextBox.Text = "Y: " + Earth.X.ToString();
            PositionZTextBox.Text = "Z: " + Earth.Z.ToString();

            Vector3D speed = Earth.Speed;

            SpeedXTextBox.Text = "X: " + speed.X.ToString();
            SpeedYTextBox.Text = "Y: " + speed.X.ToString();
            SpeedZTextBox.Text = "Z: " + speed.Z.ToString();

            MoveObj.IsEnabled = true;
            GenerateButton.IsEnabled = false;

        }


        private void viewport_MouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            camera.MoveInLookDirection(e.Delta,50);
        }

        private void Scene_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.W) { camera.MoveForward(5); }
            if (e.Key == Key.A) { camera.MoveLeft(5); }
            if (e.Key == Key.S) { camera.MoveBack(5); }
            if (e.Key == Key.D) { camera.MoveRight(5); }

        }

        private void ReturnCamera_Click(object sender, RoutedEventArgs e)
        {      
                camera.Position = new Point3D(0, 0, 40);
                camera.LookDirection = new Vector3D(0, 0, -1);
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            group.Children.Clear();
            Init();
        }

        private void AcseptButton_Click(object sender, RoutedEventArgs e)
        {
            try {

                Earth.X = double.Parse(PositionXTextBox.Text);
                Earth.Y =double.Parse(PositionYTextBox.Text);
                Earth.Z =double.Parse(PositionZTextBox.Text);
                Vector3D NewSpeed = new Vector3D(
                    double.Parse(SpeedXTextBox.Text),
                    double.Parse(SpeedYTextBox.Text),
                    double.Parse(SpeedZTextBox.Text));
                Earth.Speed = NewSpeed;

                Earth.Weight = double.Parse(WeightTextBox.Text);

            } catch {
                MessageBox.Show("Введено некоректні значення!", "Помилка!", MessageBoxButton.OK, MessageBoxImage.Error);
                group.Children.Clear();
                Init();
            }
        }

        private void Scene_MouseMove(object sender, MouseEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                do
                {
                    newPos = e.GetPosition(Scene);

                    newPos.X = newPos.X - Scene.ActualWidth / 2;
                    newPos.Y = Scene.ActualHeight / 2 - newPos.Y;

                    newPos.X = newPos.X / Scene.ActualWidth * 2;
                    newPos.Y = newPos.Y / Scene.ActualHeight * 2;

                    if (isFirst)
                    {
                        oldPos = newPos;
                        isFirst = false;
                    }

                } while ((Mouse.LeftButton != MouseButtonState.Pressed));
                ReadyForRotating = true;
            }
            else if (ReadyForRotating)
            {
                Vector3D NewPositionVector = new Vector3D(newPos.X, newPos.Y, FindZCoord(newPos.X, newPos.Y));
                Vector3D OldPositionVector = new Vector3D(oldPos.X, oldPos.Y, FindZCoord(newPos.X, oldPos.Y)); // Одиничні вектори

                //Поворот навколо Х

                degX = FindDeg(new Vector(NewPositionVector.X, NewPositionVector.Z),
                    new Vector(OldPositionVector.X, OldPositionVector.Z));

                if (oldPos.X < newPos.X) degX *= -1;

                camera.RotateAroundX(degX, 10);

                //Поворот навколо Y

                degY = FindDeg(new Vector(NewPositionVector.Z, NewPositionVector.Y),
                    new Vector(OldPositionVector.Z, OldPositionVector.Y));

                if (oldPos.Y < newPos.Y) degY *= -1;

                camera.RotateAroundY(degY, 10);

                ReadyForRotating = false;
                isFirst = true;
                degX = 0;
                degY = 0;
            }

        }

        private void PositionXTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            (sender as TextBox).Text = "";
        }

        private void PositionXTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if ((sender as TextBox).Text == "") { (sender as TextBox).Text = "0"; }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            Vector3D r = new Vector3D(Earth.X - Sun.X, Earth.Y - Sun.Y, Earth.Z - Sun.Z); // Дистанція від Землі до сонця в просторі моделювання

            r *= AUInOneVect; //знаходимо реальну відстань в Астр.Одиницях

            Vector3D F = -39.5 * ((Sun.Weight * Earth.Weight) / (Math.Pow(r.Length, 3))) * r;

            Vector3D am = F / Earth.Weight;

            Earth.Speed = Earth.Speed + (am * dt);
            Earth.MoveOn((Earth.Speed * dt) / AUInOneVect); //Переводимо з астрономічних од в СК моделювання

            PlanetsDataTextBlock.Text = Earth.PlanetName+"\n"+Earth.X.ToString() + "\n" + Earth.Y.ToString() + "\n" + Earth.Z.ToString() + "\n" + "DistToSun:\n" + (r.Length).ToString();
        }

        static double FindZCoord(double x, double Y)
        {
            return Math.Sqrt(1 - (x * x) + (Y * Y));
        }

        double FindDeg(Vector a, Vector b)
        {
            double SkalDob = a.X * b.X + a.Y * b.Y;
            return Math.Cos(SkalDob / a.Length * b.Length);
        }

        void Init()
        {

            Sun = new Planet("Сонце", 1, new Point3D(0, 0, 0), 0.8, ref group);
            Sun.SetTexture(new Uri("pack://application:,,,/images/textures/Sun.jpg", UriKind.Absolute));
            Earth = new Planet("Земля", 3.0024584e-6, new Point3D(0, 0, 0), 0.3, ref group);
            Earth.SetTexture(new Uri("pack://application:,,,/images/textures/Earth.jpg", UriKind.Absolute));
            Planet X1 = new Planet("", 1, new Point3D(1, 0, 0), 0.1, ref group);
            X1.SetColor(Color.FromRgb(255, 0, 0));
            Planet Y1 = new Planet("", 1, new Point3D(0, 1, 0), 0.1, ref group);
            Y1.SetColor(Color.FromRgb(0, 0, 255));
            Planet Z1 = new Planet("", 1, new Point3D(0, 0, 1), 0.1, ref group);
            Z1.SetColor(Color.FromRgb(0, 255, 0));

            Earth.DistanceToSunInAUVect = new Vector3D(-2.473873637660491E-01, -4.675277158909955E-05, 9.516452219046301E-01);
            Earth.MoveOn(Earth.DistanceToSunInAUVect / AUInOneVect);
            Earth.Speed = new Vector3D(-1.693102612327011E-02 * 365, 3.526242395679485E-07 * 365, -4.386551854453010E-03 * 365);

            group.Children.Add(new AmbientLight(Colors.DarkGray));
            group.Children.Add(new PointLight(Colors.White, new Point3D(0, 1, 0)));

        }

    }

}
