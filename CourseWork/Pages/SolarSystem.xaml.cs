﻿using CourseWork.Classes;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Windows.Threading;

namespace CourseWork.Pages
{
    /// <summary>
    /// Логика взаимодействия для SolarSystem.xaml
    /// </summary>
    public partial class SolarSystem : Page
    {

        Planet[] Planets = new Planet[9];
        double dt;
        double AUInOneVect = 0.005;
        static Point oldPos, newPos;
        double degX,degY;
        bool isFirst = true, ReadyForRotating = false,mDown,isPause = false;

        public SolarSystem()
        {
            InitializeComponent();
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";

            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            this.Cursor = Cursors.Arrow;
            MoveObj.IsEnabled = false;
            PauseButton.IsEnabled = false;
            ReturnCamera.Visibility = Visibility.Collapsed;
            SpeedTextBox.Text = "0.0001";
        }

        private void GenerateButton_Click(object sender, RoutedEventArgs e)
        {
            Planets[0] = new Planet("Сонце",1, new Point3D(0, 0, 0), 38, ref group,
                new Uri("pack://application:,,,/images/textures/Sun.jpg"));//The Sun
            Planets[0].DistanceToSunInAUVect = new Vector3D(0, 0, 0);
            Planets[0].Speed = new Vector3D(0, 0, 0);

            Planets[1] = new Planet("Меркурій",1.651584E-07, new Point3D(0, 0, 0), 1.36, ref group,
                new Uri("pack://application:,,,/images/textures/Mercury.jpg"));//Mercury
            Planets[1].DistanceToSunInAUVect = new Vector3D(3.004524747561470E-01, -5.045573797999990E-02, -2.801768175763694E-01);
            Planets[1].Speed = new Vector3D(1.361741395598485E-02 * 365, 5.406033720420252E-04 * 365, 2.190195366258245E-02 * 365);

            Planets[2] = new Planet("Венера",2.446958E-06, new Point3D(0, 0, 0), 3.38, ref group,
                new Uri("pack://application:,,,/images/textures/Venus.jpg"));//Venus
            Planets[2].DistanceToSunInAUVect = new Vector3D(-3.806442886141496E-01, 1.350463400072905E-02, -6.165321606325094E-01);
            Planets[2].Speed = new Vector3D(1.707004859068278E-02 * 365, -1.132135439017423E-03 * 365, -1.071846342008797E-02 * 356);

            Planets[3] = new Planet("Земля",3.002514E-06, new Point3D(0, 0, 0), 3.5, ref group,
            new Uri("pack://application:,,,/images/textures/Earth.jpg"));//Earth
            Planets[3].DistanceToSunInAUVect = new Vector3D(-2.473873637660491E-01, -4.675277158909955E-05, 9.516452219046301E-01);
            Planets[3].Speed = new Vector3D(-1.693102612327011E-02 * 365, 3.526242395679485E-07 * 365, -4.386551854453010E-03 * 365);

            Planets[4] = new Planet("Марс",3.212670E-07, new Point3D(0, 0, 0), 1.8, ref group,
                new Uri("pack://application:,,,/images/textures/Mars.jpg"));//Mars
            Planets[4].DistanceToSunInAUVect = new Vector3D(5.715616972628683E-01, 1.536995350316427E-02, 1.402508662732373E+00);
            Planets[4].Speed = new Vector3D(-1.242895272846338E-02 * 365, 4.404913383397089E-04 * 365, 6.470457211501295E-03 * 365);

            Planets[5] = new Planet("Юпітер",9.542484E-04, new Point3D(0, 0, 0), 40, ref group,
                new Uri("pack://application:,,,/images/textures/Jupiter.jpg"));//Jupiter
            Planets[5].DistanceToSunInAUVect = new Vector3D(3.065303944709429E+0, -5.168268970366246E-2, -4.068631529341944E+00);
            Planets[5].Speed = new Vector3D(5.942971871358635E-3 * 365, -1.533245951394285E-4 * 365, 4.901247768570557E-3 * 365);

            Planets[6] = new Planet("Сатурн",2.857215E-04, new Point3D(0, 0, 0), 33.7, ref group,
                new Uri("pack://application:,,,/images/textures/Saturn.jpg"));//Saturn
            Planets[6].DistanceToSunInAUVect = new Vector3D(5.507772361371580E+00, -7.438384191820355E-02, -8.329869301837219E+00);
            Planets[6].Speed = new Vector3D(4.349331287801963E-03 * 365, -2.265151247614915E-04 * 365, 3.068296731703483E-03 * 365);

            Planets[7] = new Planet("Уран",4.364505E-05, new Point3D(0, 0, 0), 14.3, ref group,
                new Uri("pack://application:,,,/images/textures/Uranus.jpg"));// Uranus
            Planets[7].DistanceToSunInAUVect = new Vector3D(1.534002089483150E+1, -1.524052595338771E-1, 1.247309779588588E+1);
            Planets[7].Speed = new Vector3D(-2.504805331610626E-3 * 365, 4.301616513280591E-5 * 365, 2.873083852736933E-3 * 365);

            Planets[8] = new Planet("Нептун",5.148316E-05, new Point3D(0, 0, 0), 13.85, ref group,
                new Uri("pack://application:,,,/images/textures/Neptune.jpg"));//Neptune
            Planets[8].DistanceToSunInAUVect = new Vector3D(2.946266154627330E+1, -5.715904352664610E-1, -5.219362536854228);
            Planets[8].Speed = new Vector3D(5.330233879881993E-4 * 365, -7.670636362019444E-5 * 365, 3.115119349642284E-3 * 365);

            for (int i = 0; i < Planets.Length; i++)
            {
                Planets[i].MoveOn(Planets[i].DistanceToSunInAUVect / AUInOneVect);
            }

            Scene.Focus();

            MoveObj.IsEnabled = true;
            PauseButton.IsEnabled = true;
            GenerateButton.IsEnabled = false;
            GenerateButton.Visibility = Visibility.Collapsed;

            ReturnCamera.Visibility = Visibility.Visible;

        }

        private void MoveObj_Click(object sender, RoutedEventArgs e)
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = new TimeSpan(0, 0, 0, 0, 1);
            timer.Start();
            Scene.Focus();
            MoveObj.IsEnabled = false;
            MoveObj.Visibility=Visibility.Collapsed;

        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (!isPause)
            {
                PlanetsPositionTextBlock.Text = "";
                for (int i = 1; i < Planets.Length; i++)
                {
                    Vector3D r = new Vector3D(Planets[i].X - Planets[0].X, Planets[i].Y - Planets[0].Y, Planets[i].Z - Planets[0].Z);
                    r *= AUInOneVect;
                    Vector3D Ful = -39.5 * ((Planets[0].Weight * Planets[i].Weight) / (Math.Pow(r.Length, 3))) * r;
                    for (int j = 1; j < Planets.Length; j++)
                    {
                        r = new Vector3D(Planets[i].X - Planets[j].X, Planets[i].Y - Planets[j].Y, Planets[i].Z - Planets[j].Z);

                        r *= AUInOneVect;
                        if (r.Length != 0)
                        {
                            Vector3D F; F = (-39.5 * (Planets[j].Weight * Planets[i].Weight) / (Math.Pow(r.Length, 3)) * r);
                            Ful = Ful + F;
                        }
                    }
                    Vector3D a = Ful / Planets[i].Weight;

                    RoundVector(ref a, 10);

                    Planets[i].Speed = Planets[i].Speed + (a * dt);
                    Planets[i].MoveOn((Planets[i].Speed * dt) / AUInOneVect);
                    PlanetsPositionTextBlock.Text += GetPlanetInf(Planets[i]);
                }
            }
        }

        private void Scene_MouseWheel(object sender, MouseWheelEventArgs e)  {
            camera.MoveInLookDirection(e.Delta);
            Scene.Focus();
        }
        private void Scene_MouseMove(object sender, MouseEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                do
                {
                    newPos = e.GetPosition(Scene);

                    newPos.X = newPos.X - Scene.ActualWidth / 2;
                    newPos.Y = Scene.ActualHeight / 2 - newPos.Y;

                    newPos.X = newPos.X / Scene.ActualWidth * 2;
                    newPos.Y = newPos.Y / Scene.ActualHeight * 2;

                    if (isFirst)
                    {
                        oldPos = newPos;
                        isFirst = false;
                    }


                } while ((Mouse.LeftButton != MouseButtonState.Pressed));
                ReadyForRotating = true;
            }
            else if(ReadyForRotating)
            {
                Vector3D NewPositionVector = new Vector3D(newPos.X, newPos.Y, FindZCoord(newPos.X, newPos.Y));
                Vector3D OldPositionVector = new Vector3D(oldPos.X, oldPos.Y, FindZCoord(newPos.X, oldPos.Y)); // Одиничні вектори
                
                double u = 0.05;

                degX = FindDeg(new Vector(NewPositionVector.X, NewPositionVector.Z),
                    new Vector(OldPositionVector.X, OldPositionVector.Z));

                if (oldPos.X < newPos.X) degX *= -1;
             
                camera.RotateAroundX(degX,10);

                //Around

                degY = FindDeg(new Vector(NewPositionVector.Z, NewPositionVector.Y),
                    new Vector(OldPositionVector.Z, OldPositionVector.Y));

                if (oldPos.Y < newPos.Y) degY *= -1;

                camera.RotateAroundY(degY,10);

                ReadyForRotating = false;
                isFirst = true;
                degX = 0;
                degY = 0;
            }
        }

        private void Scene_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.W) {camera.MoveForward();}
            if (e.Key == Key.A) {camera.MoveLeft(); }
            if (e.Key == Key.S) {camera.MoveBack(); }
            if (e.Key == Key.D) {camera.MoveRight();}
            //
            if (e.Key == Key.Left) { camera.RotateAroundX(-20);}
            if (e.Key == Key.Right) { camera.RotateAroundX(20);}
            if (e.Key == Key.Up) { camera.RotateAroundY(20);}
            if (e.Key == Key.Down) { camera.RotateAroundY(-20);}
            Scene.Focus();
        }

        private void PauseButton_Click(object sender, RoutedEventArgs e)
        {
            if (isPause)
            {
                isPause = false;
                PauseButton.Content = "Pause";
                PauseButton.Background = new SolidColorBrush(Color.FromRgb(230, 0, 66));

            }
            else
            {
                isPause = true;
                PauseButton.Content = "Remume";
                PauseButton.Background = new SolidColorBrush(Color.FromRgb(76, 98, 238));

            }
            Scene.Focus();
        }

        private void ReturnCamera_Click(object sender, RoutedEventArgs e)
        {
            camera.Position = new Point3D(0, 0, 200);
            camera.LookDirection = new Vector3D(0, 0, -1);
            Scene.Focus();
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                dt = double.Parse(SpeedTextBox.Text);
                if (dt > 0.01) dt = 0.01;
                SpeedTextBox.Text = dt.ToString();
            }
            catch
            {
                MessageBox.Show("Помилка привведенні параметрів налаштування",
                    "Помилка", MessageBoxButton.OK, MessageBoxImage.Information);
                dt = 0.0001;
                SpeedTextBox.Text = dt.ToString();
            }
            Scene.Focus();
        }

        private void Scene_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                mDown = true;
            }
            if (e.RightButton == MouseButtonState.Pressed)
            {
                mDown = false;
            }
        }

        private void Scene_Loaded(object sender, RoutedEventArgs e)
        {
            Scene.Focus();
        }

        static double FindZCoord(double x, double Y)
        {
            return Math.Sqrt(1 - (x * x) + (Y * Y));
        }

        double FindDeg(Vector a, Vector b)
        {
            double SkalDob = a.X * b.X + a.Y * b.Y;
            return Math.Cos(SkalDob / a.Length * b.Length);
        }

        string GetPlanetInf(Planet a)
        {
            Vector3D LenthToCent = new Vector3D(a.X, a.Y, a.Z);
            return $"\n\n{a.PlanetName}\nX: {a.X*AUInOneVect}\nY: {a.Y * AUInOneVect}" +
                $"\nZ: {a.Z * AUInOneVect}\nDistToSun:{LenthToCent.Length*AUInOneVect}";
        }

        void RoundVector(ref Vector3D a, int x) 
        {
            a.X = Math.Round(a.X, x);
            a.Y = Math.Round(a.Y, x);
            a.Z = Math.Round(a.Z, x);
        }
    }
}
