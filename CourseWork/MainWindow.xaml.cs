﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;


namespace CourseWork
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Cursor = Cursors.Arrow;
        }
        private void SolarSysBut_Click(object sender, RoutedEventArgs e)
        {
            Uri a = new Uri($"Pages/{(sender as Button).Name}.xaml", UriKind.Relative);
            MainFrame.Source = a;
        }
       
    }
}
