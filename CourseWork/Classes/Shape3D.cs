﻿using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace CourseWork.Classes
{
    public abstract class Shape3D
    {
        protected Point3D center;

        protected MeshGeometry3D myMesh = new MeshGeometry3D();

        protected DiffuseMaterial Material = new DiffuseMaterial(Brushes.Gray);

        protected GeometryModel3D mGeometry;

        public double X
        {
            get { return center.X; }
            set { center.X = value; }
        }
        public double Y

        {
            get { return center.Y; }
            set { center.Y = value; }
        }
        public double Z

        {
            get { return center.Z; }
            set { center.Z = value; }
        }

        protected void CrateTriangle(ref MeshGeometry3D Mesh, int i1, int i2, int i3)
        {
            Mesh.TriangleIndices.Add(i1);
            Mesh.TriangleIndices.Add(i2);
            Mesh.TriangleIndices.Add(i3);
        }
        public virtual void MoveOn(Vector3D vect)
        {
            TranslateTransform3D tranlate = new TranslateTransform3D(new Vector3D(center.X, center.Y, center.Z));

            mGeometry.Transform = tranlate;

        }
    }
}
