﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using static System.Math;

namespace CourseWork.Classes
{
    public class Sphere : Shape3D
    {
        protected int StakCount = 50, SectorCount = 50;
        protected double radius = 0.2;

        protected Model3DGroup Group;

        public double Radius
        {
            get { return radius; }
            set { radius = value; }
        }

        public void SetColor(Color Col) 
        {
            Material = new DiffuseMaterial(new SolidColorBrush(Col));

            mGeometry.Material = Material;
        }
        public void SetTexture(Uri a) 
        {
            Material = new DiffuseMaterial(new ImageBrush(new BitmapImage(a)));

            mGeometry.Material = Material;
        }

        public Sphere(ref Model3DGroup group)
        {
            int firstPoint;
            CrateSpherePoints(out firstPoint);
            CrateSphereTriangles(firstPoint);
            Group = group;
            mGeometry = new GeometryModel3D(myMesh, Material);
            mGeometry.Transform = new Transform3DGroup();
            group.Children.Add(mGeometry);

        }

        public Sphere(Point3D cent, double rad, ref Model3DGroup group)
        {
            radius = rad;
            center = cent;
            int firstPoint;
            CrateSpherePoints(out firstPoint);
            CrateSphereTriangles(firstPoint);
            Group = group;

            mGeometry = new GeometryModel3D(myMesh, Material);

            TranslateTransform3D tranlate = new TranslateTransform3D(new Vector3D(center.X, center.Y, center.Z));

            group.Children.Add(mGeometry);
            mGeometry.Transform = tranlate;
        }

        public Sphere(Point3D cent, double rad, Uri uri , ref Model3DGroup group)
        {
            radius = rad;
            center = cent;
            int firstPoint;
            CrateSpherePoints(out firstPoint);
            CrateSphereTriangles(firstPoint);
            Group = group;
            Material = new DiffuseMaterial(new ImageBrush(new BitmapImage(uri)));
            
            mGeometry = new GeometryModel3D(myMesh, Material);
            group.Children.Add(mGeometry);
        }

        private void CrateSpherePoints(out int pzero) //метот створення точок
        {
         double delta_phi = PI / StakCount;
         double delta_theta = 2 * PI / SectorCount;

            pzero = myMesh.Positions.Count;

            double phi1 = PI / 2;
            for (int p = 0; p <= StakCount; p++)
            {
                double x1 = radius * Cos(phi1);
                double y1 = radius * Sin(phi1);

                double theta = 0;
                for (int t = 0; t <= SectorCount; t++)
                {
                    myMesh.Positions.Add(new Point3D(  x1 * Cos(theta),  y1, -x1 * Sin(theta)));
                    myMesh.TextureCoordinates.Add(new Point((double)t / SectorCount, (double)p / StakCount));
                    theta += delta_theta;
                }
                phi1 -= delta_phi;
            }

        } 

        private void CrateSphereTriangles(int pt0) //метод створення трикутників з точок
        {
            int i1, i2, i3, i4;
            for (int p = 0; p <= StakCount - 1; p++)
            {
                i1 = p * (SectorCount + 1);
                i2 = i1 + (SectorCount + 1);
                for (int t = 0; t <= SectorCount - 1; t++)
                {
                    i3 = i1 + 1; i4 = i2 + 1;

                    CrateTriangle(ref myMesh, pt0 + i1, pt0 + i2, pt0 + i4);
                    CrateTriangle(ref myMesh, pt0 + i1, pt0 + i4, pt0 + i3);

                    i1 += 1; i2 += 1;
                   
                }
            }
        }

        public override void MoveOn(Vector3D vect)
        {
            center.X = center.X + vect.X;
            center.Y = center.Y + vect.Y;
            center.Z = center.Z + vect.Z;

            TranslateTransform3D tranlate = new TranslateTransform3D(new Vector3D(center.X, center.Y, center.Z));

            mGeometry.Transform = tranlate;

        }

    }
}
