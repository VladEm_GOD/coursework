﻿using System;
using System.Windows.Media.Media3D;

namespace CourseWork.Classes
{
    public class Planet : Sphere
    {
        protected double weight;
        protected Vector3D distancetosuninAU;
        protected string name;
        public string PlanetName
        {
            set { name = value; }
            get { return name; }
        }
        public double Weight
        {
            get
            {
                return weight;
            }
            set
            {
                weight = value;
            }
        }
        public Vector3D DistanceToSunInAUVect
        {
            get
            {
                return distancetosuninAU;
            }
            set
            {
                distancetosuninAU = value;
            }
        }

        protected Vector3D speed;
        public Vector3D Speed { get; set; }
        public Planet(string Name, double weig, Point3D cent, double rad, ref Model3DGroup group) : base(cent, rad, ref group)
        {
            name = Name;
            weight = weig;

        }
        public Planet(string Name, double weig, Point3D cent, double rad, ref Model3DGroup group, Uri texture)
            : base(cent, rad, texture, ref group) { weight = weig; name = Name; ; }
    }
}
