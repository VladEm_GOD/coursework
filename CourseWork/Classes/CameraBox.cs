﻿using System.Windows.Media.Media3D;

namespace CourseWork.Classes
{
    public static class CameraBox
    {
        public static void RotateAroundX(this PerspectiveCamera camera, double degX, double Sensivity = 1)
        {
            double angleD = degX * Sensivity;

            Matrix3D m = new Matrix3D();
            m.Rotate(new Quaternion(camera.UpDirection, -angleD));
            camera.LookDirection = m.Transform(camera.LookDirection);
        }
        public static void RotateAroundY(this PerspectiveCamera camera, double degY, double Sensivity = 1)
        {
            double angleD = degY * Sensivity;
            Vector3D lookDirection = camera.LookDirection;

            Vector3D cp = Vector3D.CrossProduct(camera.UpDirection, lookDirection);
            cp.Normalize();

            Matrix3D m1 = new Matrix3D();
            m1.Rotate(new Quaternion(cp, -angleD));
            camera.LookDirection = m1.Transform(camera.LookDirection);
        }

        public static void MoveInLookDirection(this PerspectiveCamera camera, double delta, double Sensivity = 20)
        {
            Vector3D cameraLookDir = camera.LookDirection;

                camera.Position = new Point3D(camera.Position.X +
                cameraLookDir.X * delta / Sensivity, camera.Position.Y +
                cameraLookDir.Y * delta / Sensivity, camera.Position.Z +
                cameraLookDir.Z * delta / Sensivity);
        }

        public static void MoveLeft(this PerspectiveCamera camera, double Step = 10) 
        {
            camera.Position = new Point3D(
            camera.Position.X + camera.LookDirection.Z * Step,
            camera.Position.Y + camera.LookDirection.Y * Step,
            camera.Position.Z + -camera.LookDirection.X * Step);

        } 
        public static void MoveRight(this PerspectiveCamera camera, double Step = 10) 
        {
            camera.Position = new Point3D(
               camera.Position.X + -camera.LookDirection.Z * Step,
               camera.Position.Y + camera.LookDirection.Y * Step,
               camera.Position.Z + camera.LookDirection.X * Step);

        }

        public static void MoveForward(this PerspectiveCamera camera, double Step = 10)
        {
            camera.Position = new Point3D(
               camera.Position.X + camera.LookDirection.X * Step,
               camera.Position.Y + camera.LookDirection.Y * Step,
               camera.Position.Z + camera.LookDirection.Z * Step);

        }

        public static void MoveBack(this PerspectiveCamera camera, double Step = 10)
        {
            camera.Position = new Point3D(
               camera.Position.X - camera.LookDirection.X * Step,
               camera.Position.Y - camera.LookDirection.Y * Step,
               camera.Position.Z - camera.LookDirection.Z * Step);

        }
    }
}
